# The packaging information for python websocket client in XiVO

This repository contains the packaging information for
[python-websocket-client](https://github.com/liris/websocket-client/releases).

To get a new version of python-websocket-client in the XiVO repository, set the
desired version in the `VERSION` file and increment the changelog.

[Jenkins](jenkins.xivo.io) will then retrieve and build the new version.
